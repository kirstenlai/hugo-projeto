import firebase from 'firebase/app'

import 'firebase/database'

const firebaseConfig = {
    apiKey: "AIzaSyChtLnpv7hMm_NS-IoF8GscqIjQcgiPPSA",
    authDomain: "gerenciador-de-evento-7b5f7.firebaseapp.com",
    databaseURL: "https://gerenciador-de-evento-7b5f7-default-rtdb.firebaseio.com",
    projectId: "gerenciador-de-evento-7b5f7",
    storageBucket: "gerenciador-de-evento-7b5f7.appspot.com",
    messagingSenderId: "1039268119604",
    appId: "1:1039268119604:web:f8141f54d906c66ca8b661"
};

var app = firebase.initializeApp(firebaseConfig)
var database = firebase.database(app)

export { database }
